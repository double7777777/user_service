CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE users(
    uuid uuid DEFAULT uuid_generate_v4 (),
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100),
    email VARCHAR(100) NOT NULL,
    password text NOT NULL,
    user_type VARCHAR(64) DEFAULT 'user',
    access_token text,
    refresh_token text,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW());

/*
insert into users(first_name,last_name,email,password,user_type,access_token) values ('Yunus','Karimboyev','karimboyevyunus@gmail.com','$2a$10$Oy4A9YbBZslWxDLXUU6VP.Szke1axQ4tPTzc00ArKMeRK3UgMARUO','superuser','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.exw43QM2_SlQ_MC3zIZ1XyUvnOldQy7CxqRYsvUI4A0');