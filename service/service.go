package service

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	p "github.com/double/user_service/genproto/post"
	u "github.com/double/user_service/genproto/user"
	"github.com/double/user_service/pkg/logger"
	grpcclient "github.com/double/user_service/service/grpc-client"
	"github.com/double/user_service/storage"
	"github.com/double/user_service/storage/repo"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"github.com/double/user_service/pkg/messagebroker"

)

type UserService struct {
	producer map[string]messagebroker.Producer
	storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Clients
}

func NewUserService(db *sqlx.DB, log logger.Logger, producer map[string]messagebroker.Producer, client grpcclient.Clients) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		Logger:  log,
		producer: producer,
		Client:  client,
	}
}

func(s *UserService) ProduceMessage(raw *p.PostRequest) error {
	data, err := raw.Marshal()
	if err != nil {
		return err
	}

	logPost := raw.String()
	fmt.Println(logPost)
	err = s.producer["user"].Produce([]byte("user"), data, logPost)
	if err != nil{
		return err
	}
	return nil
}

func (s *UserService) CreateUser(ctx context.Context, req *u.UserRequest) (*u.UserCreatedResponse, error) {
	res, err := s.storage.User().CreateUser(ctx, &repo.UserRequest{
		FirstName: req.FirstName, 
		LastName: req.LastName, 
		Email: req.Email,
		Password: req.Password,
		AccessToken: req.AccessToken,
		RefreshToken: req.RefreshToken,
	})
	fmt.Println("--------->>>", res)
	if err != nil {
		s.Logger.Error("error insert user", logger.Any("Error insert user", err))
		return &u.UserCreatedResponse{},err
	}
	return &u.UserCreatedResponse{
		Id:        res.Id,
		FirstName: res.FirstName,
		LastName:  res.LastName,
		Email:     res.Email,
		Password: res.Password,
		AccessToken: res.AccessToken,
		RefreshToken: res.RefreshToken,
		CreatedAt: res.Created_at,
	}, nil
}

func (s *UserService) GetUserById(ctx context.Context, req *u.UserId) (*u.UserResponse, error) {
	res, err := s.storage.User().GetUserById(ctx, req)
	if err != nil {
		s.Logger.Error("error get user", logger.Any("Error get user", err))
		return &u.UserResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) GetAllUsers(ctx context.Context, req *u.UserListReq) (*u.Users, error) {
	res, err := s.storage.User().GetAllUsers(ctx, req)
	if err != nil {
		s.Logger.Error("error get all user", logger.Any("error get all user", err))
		return &u.Users{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *u.UserUpdateReq) (*u.UserResponse, error) {
	res, err := s.storage.User().UpdateUser(ctx, req)
	if err != nil {
		s.Logger.Error("error update user", logger.Any("error update user", err))
		return &u.UserResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *u.UserDeleteReq) (*u.UserResponse, error) {
	res, err := s.storage.User().DeleteUser(ctx, req)
	if err != nil {
		s.Logger.Error("error delete user", logger.Any("error delete user", err))
		return &u.UserResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) GetUserByPostId(ctx context.Context, req *u.PostId) (*u.UserResponseForPost, error) {
	res, err := s.storage.User().GetUserByPostId(req.PostId)
	if err != nil {
		s.Logger.Error("error GetUserByPostId user", logger.Any("error GetUserByPostId user", err))
		return &u.UserResponseForPost{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) UserForCreate(ctx context.Context, id *u.UserId) (*u.UserResponse, error) {
	user, err := s.storage.User().UserForCreate(ctx, id)
	if err != nil {
		log.Println("failed to get user for client: ", err)
		return nil, err
	}
	return user, nil
}

func (s *UserService) CheckField(ctx context.Context, req *u.CheckRequest) (*u.CheckResponse, error){
	boolean, err := s.storage.User().CheckField(ctx, req)
	if err != nil {
		s.Logger.Error("Error checkfield user", logger.Any("error checkfield user", err))
		return &u.CheckResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return &u.CheckResponse{
		Exists: boolean.Exists,
	}, nil
}

func (s *UserService) GetByEmail(ctx context.Context, req *u.EmailRequest) (*u.LoginResponse, error){
	user, err := s.storage.User().GetByEmail(ctx, req)
	if err != nil {
		s.Logger.Error("error getbyemail", logger.Any("error ", err))
		return &u.LoginResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return user, nil
}

func (s *UserService) Login(ctx context.Context, req *u.LoginRequest) (*u.LoginResponse, error){
	req.Email = strings.ToLower(req.Email)
	req.Email = strings.TrimSpace(req.Email)

	user, err := s.storage.User().GetByEmail(ctx, &u.EmailRequest{Email: req.Email})
	if err == sql.ErrNoRows{
		s.Logger.Error("error while getting user by email Not found", logger.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.Logger.Error("error while getting user by email", logger.Error(err), logger.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password))
	if err != nil {
		s.Logger.Error("Error while comparing hashed password, Invalid credentials", logger.Any("req", req))
		return nil, status.Error(codes.InvalidArgument,"invalid credentials")
	}
	return user, nil
}

func (s *UserService) UpdateUserTokens(ctx context.Context, req *u.TokensUpdateReq) (*u.TokensUpdateRes, error){
	login, err := s.storage.User().UpdateUserTokens(ctx, req)
	if err != nil {
		s.Logger.Error("error delete user", logger.Any("error delete user", err))
		return &u.TokensUpdateRes{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return login, nil
}
