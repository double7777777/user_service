FROM golang:1.20.4-alpine
RUN mkdir user
COPY . /user
WORKDIR /user
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 8000