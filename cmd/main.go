package main

import (
	"fmt"
	"net"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	"github.com/double/user_service/config"
	u "github.com/double/user_service/genproto/user"
	"github.com/double/user_service/pkg/db"
	"github.com/double/user_service/pkg/logger"
	"github.com/double/user_service/service"
	"github.com/double/user_service/kafka"
	"github.com/double/user_service/pkg/messagebroker"
	grpcclient "github.com/double/user_service/service/grpc-client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	conf := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type: jaeger.SamplerTypeConst,
			Param: 10,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
			LocalAgentHostPort: "jaeger:6831",
		},
	}
	closer, err := conf.InitGlobalTracer(
		"user-service",
	)
	if err != nil{
		fmt.Println(err)
	}
	defer closer.Close()
	

	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	log.Info("main:sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.String("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDb, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("Error connect postgres", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println("Error connect grpc client: ", err.Error())
	}
	produceMap := make(map[string]messagebroker.Producer)
	topic := "post"
	customerTopicProduce := kafka.NewKafkaProducer(cfg, log, topic)
	defer func () {
		err := customerTopicProduce.Stop()
		if err != nil {
			log.Fatal("Failed to stopping KAFKA", logger.Error(err))
		}
	}()
	produceMap["user"] = customerTopicProduce

	userService := service.NewUserService(connDb, log, produceMap, grpcClient)

	userPort := ":8000"
	lis, err := net.Listen("tcp", userPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	u.RegisterUserServiceServer(s, userService)

	log.Info("main: server running",
		logger.String("port", cfg.UserServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}

