package kafka

import (
	"context"
	"time"

	"github.com/double/user_service/config"
	"github.com/double/user_service/pkg/logger"
	"github.com/double/user_service/pkg/messagebroker"
	kafka "github.com/segmentio/kafka-go"
)

type KafkaProduce struct {
	kafkaWriter *kafka.Writer
	log logger.Logger
}

func NewKafkaProducer(conf config.Config, log logger.Logger, topic string) messagebroker.Producer{
	connString := "kafka:9092"

	return &KafkaProduce{
		kafkaWriter: &kafka.Writer{
			Addr: kafka.TCP(connString),
			Topic: topic,
			BatchTimeout: time.Millisecond *12,
		},
		log: log,
	}
}

func(p *KafkaProduce) Start() error {
	return nil
}

func (p *KafkaProduce) Stop() error {
	err := p.kafkaWriter.Close()
	if err != nil {
		return nil
	}
	return nil
}

func (p *KafkaProduce) Produce(key, body []byte, logBody string) error {
	message := kafka.Message{
		Key: key,
		Value: body,
	}

	if err := p.kafkaWriter.WriteMessages(context.Background(), message); err != nil {
		return err
	}
	return  nil
}