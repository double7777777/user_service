package postgres

import (
	"context"
	"reflect"
	"testing"

	"github.com/double/user_service/genproto/user"
	"github.com/double/user_service/storage/repo"
)

func TestUser_Create(t *testing.T){
	tests := []struct {
		name string
		input repo.UserRequest
		want repo.UserResponse
		wantErr bool
	}{
		{
			name: "success",
			input: repo.UserRequest{
				FirstName: "Yunus",
				LastName: "Karimboyev",
				Email: "karimboyevyunus@gmail.com",
			},
			want: repo.UserResponse{
				FirstName: "Yunus",
				LastName: "Karimboyev",
				Email:  "karimboyevyunus@gmail.com",
				Created_at: "",
			},
			wantErr: false,
		},
	}
	for _, tCase := range tests{
		t.Run(tCase.name, func(t *testing.T) {
			got, err := pgRepo.CreateUser(context.Background(),&tCase.input)
			if err != nil{
				t.Fatalf("%s: excepted: %v, got %v", tCase.name, tCase.wantErr, err)
			}
			got.Id = "0"
			tCase.want.Created_at = got.Created_at

			if ! reflect.DeepEqual(tCase.want, *got){
				t.Fatalf("%s: excepted: %v, got %v", tCase.name, tCase.want, got)
			}
		})
	}
}

func TestUser_Read(t *testing.T){
	tests := []struct {
		name string
		input repo.IdRequest
		want repo.UserResponse
		wantErr bool
	}{
		{
			name: "success",
			input: repo.IdRequest{
				Id: "1",
			},
			want: repo.UserResponse{
				Id: "1",
				FirstName: "Yunus",
				LastName: "Karimboyev",
				Email: "karimboyevyunus@gmail.com",
				Created_at: "",
			},
			wantErr: false,
		},
	}
	for _, tCase := range tests{
		t.Run(tCase.name, func(t *testing.T) {
			got, err := pgRepo.GetUserById(context.Background(), &user.UserId{Id: tCase.input.Id})
			if err != nil{
				t.Fatalf("%s: excepted: %v, got %v", tCase.name, tCase.wantErr, err)
			}
			if ! reflect.DeepEqual(tCase.want, *got){
				t.Fatalf("%s: excepted: %v, got %v", tCase.name, tCase.want, got)
			}
		})
	}
}