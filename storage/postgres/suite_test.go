package postgres

import (
	"context"
	"testing"

	"github.com/double/user_service/config"
	pb "github.com/double/user_service/genproto/user"
	"github.com/double/user_service/pkg/db"
	"github.com/double/user_service/storage/repo"
	"github.com/stretchr/testify/suite"
)

type UserSuiteTest struct {
	suite.Suite
	CleanUpFunc func()
	Repo        repo.UserStorageI
}

func (s *UserSuiteTest) SetupSuite() {
	pgPool, cleanUp := db.ConnectToDBForSuite(config.Load())
	s.Repo = NewUserRepo(pgPool)
	s.CleanUpFunc = cleanUp
}

func (s *UserSuiteTest) TestUserCrud() {
	user := &pb.UserRequest{
		FirstName: "Miron",
		LastName:  "Yanovich",
	}

	createUserResp, err := s.Repo.CreateUser(context.Background(),&repo.UserRequest{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
	})
	s.Nil(err)
	s.NotNil(createUserResp)
	s.Equal(user.FirstName, createUserResp.FirstName)
	s.Equal(user.LastName, createUserResp.LastName)

	getUserResp, err := s.Repo.GetUserById(context.Background(), &pb.UserId{Id: createUserResp.Id})
	s.Nil(err)
	s.NotNil(getUserResp)
	s.Equal(getUserResp.FirstName, user.FirstName)
	s.Equal(getUserResp.LastName, user.LastName)

	updateBody := &pb.UserUpdateReq{
		Id:        createUserResp.Id,
		FirstName: "OxxxY",
		LastName:  "Miron",
	}

	updateResp, err := s.Repo.UpdateUser(context.Background(),updateBody)
	s.Nil(err)
	s.NotNil(updateResp)
	s.NotEqual(user.FirstName, updateResp.FirstName)
	s.NotEqual(user.LastName, updateResp.LastName)

	listUserResp, err := s.Repo.GetAllUsers(context.Background(),&pb.UserListReq{Page: 2, Limit: 2})
	s.Nil(err)
	s.NotNil(listUserResp)

	GetUserById, err := s.Repo.GetUserById(context.Background(),&pb.UserId{Id: createUserResp.Id})
	s.Nil(err)
	s.NotNil(GetUserById)

	deleteUserResp, err := s.Repo.DeleteUser(context.Background(), &pb.UserDeleteReq{Id: createUserResp.Id})
	s.Nil(err)
	s.NotNil(deleteUserResp)

}

func (suite *UserSuiteTest) TearDownSuite() {
	suite.CleanUpFunc()
}

func TestUserRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(UserSuiteTest))
}