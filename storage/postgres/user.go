package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"
	"github.com/opentracing/opentracing-go"
	u "github.com/double/user_service/genproto/user"
	"github.com/double/user_service/storage/repo"
)

func (r *UserRepo) CreateUser(ctx context.Context, user *repo.UserRequest) (*repo.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateUser")
	defer trace.Finish()
	var res repo.UserResponse
	fmt.Println(user)
	err := r.db.QueryRow(`
	INSERT INTO 
	  users(first_name, last_name, email,password, access_token, refresh_token) 
	VALUES
	  ($1, $2, $3, $4, $5, $6) 
	RETURNING 
	  id, first_name, last_name, email, password, access_token, refresh_token, created_at`,
		user.FirstName, user.LastName, user.Email, user.Password, user.AccessToken, user.RefreshToken).
		Scan(
		&res.Id, 
		&res.FirstName, 
		&res.LastName, 
		&res.Email,
		&res.Password,
		&res.AccessToken,
		&res.RefreshToken, 
		&res.Created_at)
	if err != nil {
		log.Println("Error inserting user info")
		return &repo.UserResponse{}, err
	}
	return &res, nil
}

func (r *UserRepo) GetUserById(ctx context.Context, user *u.UserId) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserById")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
	SELECT
		id, first_name, last_name, email, created_at, updated_at
	FROM 
		users
	WHERE
		id=$1 AND deleted_at IS NULL
	`, user.Id).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.Email, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		return &u.UserResponse{}, err
	}
	return &res, nil
}

func (r *UserRepo) GetAllUsers(ctx context.Context, user *u.UserListReq) (*u.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetAllUsers")
	defer trace.Finish()
	var res u.Users
	offset := (user.Page - 1) * user.Limit

	rows, err := r.db.Query(`
	SELECT id, first_name, last_name, email, created_at, updated_at
	FROM 
		users
	WHERE
	deleted_at is null limit $1 offset $2`, user.Limit, offset)
	if err != nil {
		return &u.Users{}, err
	}
	for rows.Next() {
		temp := u.UserResponse{}
		err = rows.Scan(
			&temp.Id, 
			&temp.FirstName, 
			&temp.LastName, 
			&temp.Email, 
			&temp.CreatedAt, 
			&temp.UpdatedAt)

		if err != nil {
			return &u.Users{}, err
		}
		res.Users = append(res.Users, &temp)
	}
	return &res, nil
}

func (r *UserRepo) UpdateUser(ctx context.Context, user *u.UserUpdateReq) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdateUser")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
	UPDATE 
		users 
	SET
		first_name = $1, last_name = $2, email = $3, updated_at = NOW()
	WHERE 
		id = $4 AND deleted_at IS NULL
	RETURNING 
		id, first_name, last_name, email, created_at, updated_at
	`, user.FirstName, user.LastName, user.Email, user.Id).Scan(
		&res.Id, 
		&res.FirstName, 
		&res.LastName, 
		&res.Email, 
		&res.CreatedAt, 
		&res.UpdatedAt)

	if err != nil {
		return &u.UserResponse{}, err
	}
	return &res, nil
}

func (r *UserRepo) DeleteUser(ctx context.Context, user *u.UserDeleteReq) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "DeleteUser")
	defer trace.Finish()
	temp := u.UserResponse{}
	
	err := r.db.QueryRow(`
	UPDATE
		users
	SET
		deleted_at = $1
	WHERE
		id = $2
	RETURNING
	id, first_name, last_name, email, created_at, updated_at`, time.Now(), user.Id).Scan(
		&temp.Id, 
		&temp.FirstName, 
		&temp.LastName, 
		&temp.Email, 
		&temp.CreatedAt, 
		&temp.UpdatedAt,
	)

	if err != nil {
		return &u.UserResponse{}, nil
	}
	return &temp, nil
}

func (r *UserRepo) GetUserByPostId(id string) (*u.UserResponseForPost, error) {
	res := u.UserResponseForPost{}
	err := r.db.QueryRow(`
	SELECT 
	  id, post_id, first_name, created_at
	FROM 
	  users 
	WHERE 
	  post_id=$1`, id).Scan(&res.Id, &res.PostId, &res.FirstName, &res.CreatedAt)
	if err != nil {
		return &u.UserResponseForPost{}, err
	}
	return &res, nil
}

func (r *UserRepo) UserForCreate(ctx context.Context, id *u.UserId) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UserForCreate")
	defer trace.Finish()
	resp := u.UserResponse{}
	err := r.db.QueryRow("SELECT id, first_name, last_name, email, created_at, updated_at FROM users WHERE id = $1", id.Id).Scan(
		&resp.Id, 
		&resp.FirstName, 
		&resp.LastName,
		&resp.Email, 
		&resp.CreatedAt, 
		&resp.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}
	return &resp, nil

}

func (r *UserRepo) CheckField(ctx context.Context, req *u.CheckRequest) (*u.CheckResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CheckField")
	defer trace.Finish()
	query := fmt.Sprintf("SELECT 1 FROM users WHERE %s = $1", req.Field)
	var exists int
	err := r.db.QueryRow(query, &req.Value).Scan(&exists)
	fmt.Println("err>>>>",err)
	if err == sql.ErrNoRows{
		return &u.CheckResponse{Exists: false}, nil
	}

	if err != nil {
		return &u.CheckResponse{}, err
	}

	if exists == 0 {
		return &u.CheckResponse{Exists: false}, nil
	}
	return &u.CheckResponse{Exists: true}, nil
}

func (r *UserRepo) GetByEmail(ctx context.Context, req *u.EmailRequest) (*u.LoginResponse, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetByEmail")
	defer trace.Finish()
	res := u.LoginResponse{}

	err := r.db.QueryRow(`SELECT 
		id,
		first_name,
		last_name,
		email,
		password,
		user_type,
		refresh_token,
		created_at,
		updated_at
	FROM users WHERE email = $1 and deleted_at is null`, req.Email).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Email,
		&res.Password,
		&res.UserType,
		&res.RefreshToken,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	if err != nil {
		fmt.Println("error while getting login user")
		return &u.LoginResponse{}, err
	}

	return &res, nil
}


func (r *UserRepo) UpdateUserTokens(ctx context.Context, req *u.TokensUpdateReq) (*u.TokensUpdateRes, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdateUserTokens")
	defer trace.Finish()
	res := u.TokensUpdateRes{}

	err := r.db.QueryRow(`UPDATE users SET  refresh_token = $1 WHERE id = $2
	RETURNING id, refresh_token, updated_at`, req.RefreshToken, req.Id).Scan(
		&res.Id,
		&res.RefreshToken,
		&res.UpdatedAt,
	)
	if err != nil {
		fmt.Println("error while updated user token")
		return &u.TokensUpdateRes{}, err
	}
	return &res, nil
}