package postgres

import (
	"log"
	"os"
	"testing"

	"github.com/double/user_service/config"
	"github.com/double/user_service/pkg/db"
	"github.com/double/user_service/pkg/logger"
)



var pgRepo *UserRepo

func TestMain(m *testing.M){
	conf := config.Load()
	connDB, err := db.ConnectToDB(conf)
	if err != nil{
		log.Fatal("Error while to connect db", logger.Error(err))
	}
	pgRepo = NewUserRepo(connDB)
	os.Exit(m.Run())
}

