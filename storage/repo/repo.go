package repo

import (
	"context"

	u "github.com/double/user_service/genproto/user"
)

type UserStorageI interface {
	CreateUser(context.Context,*UserRequest) (*UserResponse, error)
	GetUserById(context.Context,*u.UserId) (*u.UserResponse, error)
	GetAllUsers(context.Context,*u.UserListReq) (*u.Users, error)
	UpdateUser(context.Context,*u.UserUpdateReq) (*u.UserResponse, error)
	DeleteUser(context.Context,*u.UserDeleteReq) (*u.UserResponse, error)
	GetUserByPostId(id string) (*u.UserResponseForPost, error)
	UserForCreate(context.Context,*u.UserId) (*u.UserResponse, error)
	CheckField(context.Context,*u.CheckRequest) (*u.CheckResponse, error)
	GetByEmail(context.Context,*u.EmailRequest) (*u.LoginResponse, error)
	// Login(*u.LoginRequest) (*u.LoginResponse, error)
	UpdateUserTokens(context.Context,*u.TokensUpdateReq) (*u.TokensUpdateRes, error)
}

type UserRequest struct {
	FirstName    string
	LastName     string
	Email        string
	Password     string
	AccessToken  string
	RefreshToken string
}

type UserResponse struct {
	Id           string
	FirstName    string
	LastName     string
	Email        string
	Password     string
	AccessToken  string
	RefreshToken string
	Created_at   string
}

type IdRequest struct {
	Id string
}
